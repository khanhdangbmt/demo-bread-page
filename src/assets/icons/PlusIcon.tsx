export const PlusIcon = () => {
  return (
    <svg
      width={"1em"}
      height={"1em"}
      enable-background="new 0 0 15 15"
      viewBox="0 0 15 15"
      xmlns="http://www.w3.org/2000/svg"
      fill="currentColor"
    >
      <path d="m8 7v-7h-1v7h-7v1h7v7h1v-7h7v-1z"></path>
    </svg>
  );
};

const headerLinkWithBorder = (title: string) => {
  return (
    <div className="uppercase cursor-pointer border-b-2 border-transparent border-solid hover:border-[#da1a32] pb-1">
      {title}
    </div>
  );
};

export const HeaderLink = () => {
  return (
    <div className="mt-[30px] mb-3">
      <div className="container flex flex-row gap-[10px] md:gap-[50px] px-2">
        {["Shop", "Receipt", "Learn", "About", "Blog"].map((item) => {
          return headerLinkWithBorder(item);
        })}
      </div>
    </div>
  );
};

import { ClockIcon } from "@/assets/icons/ClockIcon";
import { PlusIcon } from "@/assets/icons/PlusIcon";
import { PrintIcon } from "@/assets/icons/PrintIcon";
import { YieldIcon } from "@/assets/icons/YieldIcon";
import { Grid, Breadcrumbs, Link } from "@mui/material";

import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import { Button } from "../ui";

const Receipt = ({ name, time }: { name: string; time: string }) => {
  return (
    <div>
      <div className="text-sm mb-[10px] uppercase">{name}</div>
      <div className="font-semibold text-2xl">{time}</div>
    </div>
  );
};

const breadcrumbs = [
  <Link fontWeight={700} underline="hover" key="1" color="inherit" href="#">
    Receipt
  </Link>,
  <Link fontWeight={700} underline="hover" key="2" color="inherit" href="#">
    Bread
  </Link>,
  <Link fontWeight={700} underline="hover" key="2" color="inherit" href="#">
    Quick Bread
  </Link>,
  <div key="4"></div>,
];

export const WholeGrainBananaBread = () => {
  return (
    <>
      <Breadcrumbs
        separator={<NavigateNextIcon color="error" fontSize="small" />}
        aria-label="breadcrumb"
      >
        {breadcrumbs}
      </Breadcrumbs>

      <div className="mt-[30px] text-3xl md:text-6xl font-semibold">
        Whole-Grain Banana bread
      </div>
      <div className="mt-10 md:mt-[100px] text-2xl mb-[30px]">
        This one-bowl banana bread - our 2018 Recipe of the Year - uses the
        simplest ingredients, but is incredibly moist and flavorful. While the
        recipe calls for a 50/50 mix of flours (all-purpose and whole wheat), we
        often make the bread 100% whole wheat, and honestly No one can tell,
        it&apos;s that good! And not only is this bread delicious - it&apos;s
        versatile.
      </div>

      <div className="pb-[25px] border-b border-solid border-[#e4e4e4] flex gap-[10px] mb-[35px]">
        <Grid container>
          <Grid item sm={4} xs={6}>
            <div className="flex gap-[10px]">
              <div className="text-4xl flex items-center">
                <ClockIcon />
              </div>

              <Receipt name="Prep" time="10 mins" />
            </div>
          </Grid>
          <Grid item sm={4} xs={6}>
            <Receipt name="Bake" time="1 hr to 1 hr 15 mins" />
          </Grid>
          <Grid item sm={4} xs={6}>
            <Receipt name="Total" time="1 hr 25 mins" />
          </Grid>
        </Grid>
      </div>

      <div>
        <Grid container>
          <Grid item xs={12} sm={6}>
            <div className="flex gap-[10px]">
              <div className="text-4xl flex items-center">
                <YieldIcon />
              </div>

              <Receipt name="Yield" time="1 loaf, 12 generous servings" />
            </div>
          </Grid>
          <Grid item xs={12} sm={6} className="flex gap-2 !mt-[20px] md:!mt-0">
            <Button className="text-nowrap">
              <div className="text-base">
                <PlusIcon />
              </div>
              Save Recipe
            </Button>

            <Button>
              <div className="text-base">
                <PrintIcon />
              </div>
              Print
            </Button>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

import { ExpandLess, ExpandMore, StarBorder } from "@mui/icons-material";
import {
  List,
  ListSubheader,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Collapse,
} from "@mui/material";
import React from "react";
import { BurgerIcon } from "../ui/BurgerIcon/BurgerIcon";

const headerMenuItem = (title: string) => {
  return <div className="uppercase pt-3 pb-[18px]">{title}</div>;
};

export const Header = () => {
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <div className="bg-[#F8F5F0] py-3 md:pl-10 sticky px-2 md:px-0">
      <div className="container gap-[30px] px-2 !hidden md:!flex">
        {["Category", "Collections", "Resources"].map((item) => {
          return headerMenuItem(item);
        })}
      </div>
      <List
        className="md:hidden"
        sx={{ width: "100%" }}
        component="nav"
        aria-labelledby="nested-list-subheader"
      >
        <div onClick={handleClick} className="flex justify-end">
          <BurgerIcon />
        </div>
        <Collapse in={open} timeout="auto" unmountOnExit>
          {["Category", "Collections", "Resources"].map((item) => {
            return headerMenuItem(item);
          })}
        </Collapse>
      </List>
    </div>
  );
};

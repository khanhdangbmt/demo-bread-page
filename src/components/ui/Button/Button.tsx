import { cn } from "@/utils/utils";

export const Button = ({ children, className }: any) => {
  return (
    <button
      className={cn(
        "px-[20px] py-[10px] flex gap-[10px]  border border-[#da1a32] hover:border-black hover:text-[#da1a32] h-fit",
        className
      )}
    >
      {children}
    </button>
  );
};

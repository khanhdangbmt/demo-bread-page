import { useState, useEffect } from "react";
import "./style.css";
import { cn } from "@/utils/utils";

export const BurgerIcon = () => {
  const [isActive, setIsActive] = useState(false);

  const toggleMenu = () => {
    const isActiveVal = !isActive;
    setIsActive(isActiveVal);
  };

  return (
    <div
      className={cn(isActive ? `${""} ${"change"}` : "", "w-fit")}
      onClick={toggleMenu}
    >
      <div className={"bar1"}></div>
      <div className={"bar2"}></div>
      <div className={"bar3"}></div>
    </div>
  );
};

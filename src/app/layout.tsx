import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { AppRouterCacheProvider } from "@mui/material-nextjs/v13-appRouter";
import "./globals.css";
import { cn } from "@/utils/utils";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import localFont from "next/font/local";

const inter = Inter({ subsets: ["latin"] });

const brandonFont = localFont({
  src: [
    {
      weight: "100",
      path: "../assets/fonts/Brandon_thin.otf",
    },
    {
      path: "../assets/fonts/Brandon_reg.otf",
      weight: "400",
    },
    { path: "../assets/fonts/Brandon_med.otf", weight: "600" },
    { path: "../assets/fonts/Brandon_bld.otf", weight: "700" },
  ],
  variable: "--fon-brandon",
});

const rabbidFont = localFont({
  src: "../assets/fonts/Rabbid_Highway_Sign_IV.otf",
  variable: "--fon-rabbid",
});

export const metadata: Metadata = {
  title: "Bread Page",
  description: "Khanhdangbmt",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body
        className={cn(
          inter.className,
          brandonFont.className,
          "overflow-hidden pb-14"
        )}
      >
        <AppRouterCacheProvider>{children}</AppRouterCacheProvider>
      </body>
    </html>
  );
}

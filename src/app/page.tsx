"use client";

import { Grid, Link } from "@mui/material";
import { HeaderLink, Header } from "@/components/Homepage";
import { WholeGrainBananaBread } from "@/components/Homepage/WholeGrainBananaBread";
import Image from "next/image";

export default function Home() {
  return (
    <section className="text-black">
      <HeaderLink />
      <Header />

      <div className="mt-11 container">
        <Grid container spacing={4} className="px-2">
          <Grid item xs={12} md={6}>
            <WholeGrainBananaBread />
          </Grid>
          <Grid item xs={12} md={6}>
            <Image
              alt="image-bread"
              src={"/static/images/image.png"}
              width={0}
              height={0}
              sizes="100vw"
              loading="lazy"
              style={{ width: "100%", height: "auto" }}
            />
          </Grid>
        </Grid>
      </div>
    </section>
  );
}

# Bread Page

- Mobile support for 320px

## Desktop Version
![alt text](bread_page_desktop.png)

## Mobile Version
![alt text](bread_page_mobile.png)

## How to run source
```
1. clone repo
2. pnpm i
3. pnpm run dev
```